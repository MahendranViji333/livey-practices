import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "home",
    component: () =>
      import(/* webpackChunkName: "Home view" */ "../views/HomeView.vue"),
  },
  {
    path: "/device",
    name: "Device",
    component: () =>
      import(/* webpackChunkName: "Home view" */ "../views/DevicePage.vue"),
  },
  {
    path: "/signup",
    name: "signup",
    component: () =>
      import(/* webpackChunkName: "Home view" */ "../views/SignUp.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
